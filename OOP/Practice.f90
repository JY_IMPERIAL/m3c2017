Program practise

implicit none 
integer :: N,i   ! N number of intervals, i the account number
real(kind=8):: dx, sum, x, fx

N=20
sum=0
dx=1.0/dble(N)

do i=1,N
	x=(dble(i)-0.5)*dx
	fx=4.0/(1+x*x)
  sum=sum+fx*dx
  print *, 'eachstep  sum', fx*dx,sum
end do 

end program practise