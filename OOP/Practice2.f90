Program practise

implicit none 
integer :: N,i   ! N number of intervals, i the account number
real(kind=8):: dx, sum, x, fx

N=20
sum=0
dx=1.0/dble(N)

do i=1,N
	x=(dble(i)-0.5)*dx
	call product(x,fx)
  sum=sum+fx*dx
  print *, 'eachstep  sum', fx*dx,sum
end do 

end program practise


subroutine product(x, fx)
implicit none

real(kind=8), intent(in)  :: x
real(kind=8), intent(out) :: fx

fx=4.0/(1+x*x)
end subroutine product

