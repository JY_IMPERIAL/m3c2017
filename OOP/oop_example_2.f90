module class_Circle
  implicit none
  private
  real(kind=8):: pi=3.1415926  ! constant value used within the class   

type, public :: Circle 
  real(kind=8):: radius        !use could define the values
 contains
  procedure :: area=>circle_area
  procedure :: print=>circle_print
end type Circle

contains
function circle_area (this) result(area)
  class(Circle),intent(in):: this
  real(kind=8) ::  area
  area=pi*this%radius**2
end function circle_area

subroutine circle_print(this)
 class(Circle),intent(in) :: this
 real(kind=8) :: area 
 area=this%area()
 print *, "Circle:r= ", this%radius, "area= ", area
end subroutine circle_print
end module class_Circle

program circle_test1
  use class_Circle
  implicit none 
  type(Circle) :: YUAN
  !YUAN=Circle(100)
  YUAN%radius=50
  call YUAN%print 
end program circle_test1





